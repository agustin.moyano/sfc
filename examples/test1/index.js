const {sfc} = require('../../lib/index.js');

const {Transform, Readable} = require('stream');

let i = 0;
let l = 1000000;
let start = new Readable({
    objectMode: true,
    end: false,
    read(bytes) {
        let _doRead = () => {
            let goOn = true;
            while(i < l) {
                goOn = this.push(i++);
                if(!goOn) return;
            }
            if(i >= l) {
                console.log('!!!! WROTE ALL !!!!');
                this.push(null);
            }
        };
        _doRead();
    }
});

start.on('pause', ()=>{
    console.log('#### HALT!!! ####');
});
start.on('resume', () => {
    console.log('==== GO ON ====');
});

let end = new Transform({
    readableObjectMode: true,
    writableObjectMode: true,
    transform(chunk, encoding, cb) {
        this.push(chunk+'\n');
        cb();
    }
});

sfc.pause().parseFiles('./sfc/conf.yml');

start.pipe(sfc.get('Goal', 'demo')).resolve(end).pipe(process.stdout);