const {sfc} = require('../../lib/index.js');

sfc.pause().parseFiles('./sfc/conf.yml');

function countDown(n) {
    if(n > 0) {
        console.log('Reading in '+n+' seconds');
        return setTimeout(()=>countDown(--n), 1000);
    }
    sfc.resume();
}

countDown(10);