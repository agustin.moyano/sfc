const express = require('express')
const app = express()
const port = 3000
const bodyParser = require('body-parser')
const {sfc} = require('../../lib/index')
sfc.parseFiles('./sfc/conf.yml')
const private = express.Router()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use('/private',private)

private.use((req, res, next) =>{
  //req.pipe(res)
  req.pipe(sfc.get('Goal', 'authenticate')).callback(next)
})

private.use('/:id', (req, res, next) => {
  req.pipe(sfc.get('Goal', 'authorise')).callback(next);
})

private.get('/:id/data', (req, res, next) => {
  req.pipe(sfc.get('Transform', 'get_user')).on('error', next).pipe(res);
});

private.post('/:id/data', (req, res, next) => {
  req.pipe(sfc.get('Transform', 'get_user')).on('error', next).pipe(res);
});

app.get('/', (req, res) => {
  res.send('Hola Público')
})

app.use(function(err, req, res, next) {
  console.error(err);
  res.status(err.code).send(err.message);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})