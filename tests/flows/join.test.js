const {FlowJoin} = require('../../lib');
const {Readable, Writable} = require('stream');

describe('FlowJoin test', () => {

    test('Joins flow from multiple sources until end arrives', done => {
        const start1 = Readable.from(['Hello', 'Bye bye']);
        start1.options = {name: 'start1'};
        const start2 = Readable.from(['World', 'Moon']);
        start2.options = {name: 'start2'};

        let results = [];

        const checkFinish = () => {
            expect(results).toEqual([{start1: 'Hello', start2: 'World'}, {start1: 'Bye bye', start2: 'Moon'}]);
            done();
        };

        const writeStream = new Writable({
            objectMode: true,
            write(chunk, encoding, callback) {
                results.push(chunk);
                callback();
            }
        });
        writeStream.on('finish', checkFinish);
        
        const flow = new FlowJoin();
        flow.pipe(writeStream);
        start1.pipe(flow);
        start2.pipe(flow);
    });

});