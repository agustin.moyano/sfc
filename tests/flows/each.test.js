const {FlowEach} = require('../../lib');
const {Readable, Transform, Writable} = require('stream');

describe('FlowEach test', () => {

    test('Flows each element of array', done => {
        const payload = ['message1', 'message2', 'message3'];

        const start = new Readable({
            objectMode: true,
            read() {
                this.push(payload);
                this.push(null);
            }
        });

        const results = [];
        let count = 0;

        const countStream = new Transform({
            transform(chunk, encoding, callback) {
                count++;
                this.push(chunk);
                callback();
            }
        });

        const checkFinish = () => {
            expect(results).toEqual(payload);
            expect(count).toBe(3);
            done();
        };
        const writeStream = new Writable({
            write(chunk, encoding, callback) {
                results.push(chunk.toString());
                callback();
            }
        });
        writeStream.on('finish', checkFinish);
        
        const flow = new FlowEach();
        start.pipe(flow).pipe(countStream).pipe(writeStream);
    });

});