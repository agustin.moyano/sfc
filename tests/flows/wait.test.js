const {FlowWait} = require('../../lib');
const {Readable, Writable} = require('stream');

describe('FlowWait test', () => {

    test('Waits until every source ends to emit end', done => {
        const start1 = new Readable({
            objectMode: true,
            read() {
                this.push('Hello');
                this.push(null);
            }
        });
        const start2 = new Readable({
            objectMode: true,
            read() {
                setTimeout(() => {
                    this.push('World');
                    this.push(null);
                }, 500);
            }
        });

        let results = [];

        const checkFinish = () => {
            expect(results).toEqual(['Hello', 'World']);
            done();
        };

        const writeStream = new Writable({
            objectMode: true,
            write(chunk, encoding, callback) {
                results.push(chunk);
                callback();
            }
        });
        writeStream.on('finish', checkFinish);
        
        const flow = new FlowWait();
        flow.pipe(writeStream);
        start1.pipe(flow);
        start2.pipe(flow);
    });

});