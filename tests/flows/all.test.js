const {FlowAll} = require('../../lib');
const {Readable, Writable} = require('stream');

describe('FlowAll test', () => {

    test('Flows to all matching streams', done => {
        const start = Readable.from(['message']);
        const results = {};
        const checkFinish = () => {
            for(let i of [match1Stream, match2Stream, noMatchStream]) {
                if(!i._writableState.ended) {
                    return;
                }
            }
            expect(results).toEqual({match1Stream: 'message', match2Stream: 'message'});
            expect(Object.keys(results)).not.toContain('noMatchStream');
            done();
        };
        const match1Stream = new Writable({
            write(chunk, encoding, callback) {
                results['match1Stream'] = chunk.toString();
                callback();
            }
        });
        match1Stream.on('finish', checkFinish);
        const match2Stream = new Writable({
            write(chunk, encoding, callback) {
                results['match2Stream'] = chunk.toString();
                callback();
            }
        });
        match2Stream.on('finish', checkFinish);
        const noMatchStream = new Writable({
            write(chunk, encoding, callback) {
                results['noMatchStream'] = chunk.toString();
                callback();
            }
        });
        noMatchStream.on('finish', checkFinish);

        const flow = new FlowAll();
        flow.when(payload => payload === 'message', match1Stream);
        flow.when(payload => payload !== 'message', noMatchStream);
        flow.when(payload => /message/.test(payload), match2Stream);
        start.pipe(flow);
    });

    test('Also pipe unconditionally', done => {
        const start = Readable.from(['message']);
        const results = {};
        const checkFinish = () => {
            for(let i of [match1Stream, noneStream, noMatchStream]) {
                if(!i._writableState.ended) {
                    return;
                }
            }
            expect(results).toEqual({match1Stream: 'message', noneStream: 'message'});
            expect(Object.keys(results)).not.toContain('noMatchStream');
            done();
        };
        const match1Stream = new Writable({
            write(chunk, encoding, callback) {
                results['match1Stream'] = chunk.toString();
                callback();
            }
        });
        match1Stream.on('finish', checkFinish);
        const noneStream = new Writable({
            write(chunk, encoding, callback) {
                results['noneStream'] = chunk.toString();
                callback();
            }
        });
        noneStream.on('finish', checkFinish);
        const noMatchStream = new Writable({
            write(chunk, encoding, callback) {
                results['noMatchStream'] = chunk.toString();
                callback();
            }
        });
        noMatchStream.on('finish', checkFinish);

        const flow = new FlowAll();
        flow.when(payload => payload === 'message', match1Stream);
        flow.when(payload => payload !== 'message', noMatchStream);
        flow.pipe(noneStream);
        start.pipe(flow);
    });

    test('Pipe to none when no cond matches', done => {
        const start = Readable.from(['message']);
        const results = {};
        const checkFinish = () => {
            for(let i of [noneStream, noMatchStream]) {
                if(!i._writableState.ended) {
                    return;
                }
            }
            expect(results).toEqual({noneStream: 'message'});
            expect(Object.keys(results)).not.toContain('noMatchStream');
            done();
        };
        const noneStream = new Writable({
            write(chunk, encoding, callback) {
                results['noneStream'] = chunk.toString();
                callback();
            }
        });
        noneStream.on('finish', checkFinish);
        const noMatchStream = new Writable({
            write(chunk, encoding, callback) {
                results['noMatchStream'] = chunk.toString();
                callback();
            }
        });
        noMatchStream.on('finish', checkFinish);

        const flow = new FlowAll();
        flow.when(payload => payload !== 'message', noMatchStream);
        flow.none(noneStream);
        start.pipe(flow);
    });

});