const {FlowHold} = require('../../lib');
const {Readable, Transform, Writable} = require('stream');

describe('FlowHold test', () => {

    test('Holds flow from multiple sources until end arrives', done => {
        const start = Readable.from(['message1', 'message2', 'message3']);

        let results = {};
        let calls = 0;
        
        const transform1 = new Transform({
            objectMode: true,
            transform(chunk, encoding, callback) {
                this.push('tr1: '+chunk.toString()+'!');
                callback();
            }
        });
        transform1.options = {name: 'transform1'};
        const transform2 = new Transform({
            objectMode: true,
            transform(chunk, encoding, callback) {
                this.push('tr2: '+chunk.toString()+'?');
                callback();
            }
        });
        transform2.options = {name: 'transform2'};

        const checkFinish = () => {
            expect(results).toEqual({transform1: ['tr1: message1!', 'tr1: message2!', 'tr1: message3!'], transform2: ['tr2: message1?', 'tr2: message2?', 'tr2: message3?']});
            expect(calls).toBe(1);
            done();
        };

        const writeStream = new Writable({
            objectMode: true,
            write(chunk, encoding, callback) {
                results = {...results, ...chunk};
                calls++;
                callback();
            }
        });
        writeStream.on('finish', checkFinish);
        
        const flow = new FlowHold();
        flow.pipe(writeStream);
        start.pipe(transform1).pipe(flow);
        start.pipe(transform2).pipe(flow);
    });

});