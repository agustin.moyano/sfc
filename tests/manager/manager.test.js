const {sfc, Duplex} = require('../../lib');
const path = require('path');
const fs = require('fs');

describe('Manager tests', () => {

    beforeEach(() => {
        sfc.clean();
    });

    test('Get a stream', (done) => {
        sfc.pause().parseFiles(path.join(__dirname,'./test1.sfc.yml'));
        const stream = sfc.get('Duplex', 'duplex');
        stream.on('finish', () => done());
        expect(stream).toBeInstanceOf(Duplex);
        sfc.resume();
    });

    test('Check file results', (done) => {
        try {
            fs.unlinkSync(path.join(__dirname, 'manager2.out'));
        } catch(e) {}
        sfc.pause().parseFiles(path.join(__dirname,'./test2.sfc.yml'));
        const stream = sfc.get('write_file', 'write_file');
        stream.on('finish', () => {
            fs.readFile(path.join(__dirname, 'manager2.out'), (err, data) => {
                expect(data.toString()).toMatch('0 is even');
                expect(data.toString()).toMatch('1 is odd');
                expect(data.toString()).toMatch('18 is even');
                expect(data.toString()).toMatch('19 is odd');
                done();
            });
        });
        sfc.resume();
    });
});