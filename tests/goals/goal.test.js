const {Goal} = require('../../lib');
const {Readable, Writable} = require('stream');
const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');
const confStr = fs.readFileSync(path.join(__dirname,'./goal_children.yml'));
const goalChildren = yaml.safeLoadAll(confStr)

describe('Goal stream tests', () => {

    test('Pipe to corresponding stream', done => {
        const start = Readable.from(['message']);
        const goal = new Goal();
        goal.build(goalChildren);
        const results = {};
        const checkFinish = () => {
            for(let i of [resolvedStream, rejectedStream]) {
                if(!i._writableState.ended) {
                    return;
                }
            }
            expect(results).toEqual({resolvedStream: 'message', rejectedStream: 'message'});
            done();
        };
        const resolvedStream = new Writable({
            write(chunk, encoding, callback) {
                results['resolvedStream'] = chunk.toString();
                callback();
            }
        });
        resolvedStream.on('finish', checkFinish);
        const rejectedStream = new Writable({
            write(chunk, encoding, callback) {
                results['rejectedStream'] = chunk.toString();
                callback();
            }
        });
        rejectedStream.on('finish', checkFinish);
        goal.resolve(resolvedStream);
        goal.reject(rejectedStream);
        start.pipe(goal);
    });

    test('Manage exceptions', done => {
        const start = Readable.from(['throw']);
        const goal = new Goal();
        goal.build(goalChildren);
        const results = {};
        const checkFinish = () => {
            for(let i of [resolvedStream, rejectedStream]) {
                if(!i._writableState.ended) {
                    return;
                }
            }
            expect(results).toEqual({rejectedStream: 'A thrown Error'});
            done();
        };
        const resolvedStream = new Writable({
            write(chunk, encoding, callback) {
                results['resolvedStream'] = chunk.toString();
                callback();
            }
        });
        resolvedStream.on('finish', checkFinish);
        const rejectedStream = new Writable({
            write(chunk, encoding, callback) {
                results['rejectedStream'] = chunk.toString();
                callback();
            }
        });
        rejectedStream.on('finish', checkFinish);
        goal.resolve(resolvedStream);
        goal.reject(rejectedStream);
        start.pipe(goal);
    });

});

describe('Goal promise tests', () =>{
    
    test('Resolve promise', done => {
        const start = Readable.from(['resolve']);
        const goal = new Goal();
        goal.build(goalChildren);
        
        goal.then(message => {
            expect(message).toBe('resolve');
            done();
        });
        
        start.pipe(goal);
    });

    test('Reject promise', done => {
        const start = Readable.from(['reject']);
        const goal = new Goal();
        goal.build(goalChildren);
        
        goal.catch(message => {
            expect(message).toBe('reject');
            done();
        });
        
        start.pipe(goal);
    });

    test('Manage exceptions', done => {
        const start = Readable.from(['throw']);
        const goal = new Goal();
        goal.build(goalChildren);
        
        goal.catch(message => {
            expect(message).toBe('A thrown Error');
            done();
        });
        
        start.pipe(goal);
    });

});

describe('Goal callback tests', () => {

    test('Resolve promise', done => {
        const start = Readable.from(['resolve']);
        const goal = new Goal();
        goal.build(goalChildren);
        
        goal.callback((error, message) => {
            expect(error).toBe(null);
            expect(message).toBe('resolve');
            done();
        });
        
        start.pipe(goal);
    });

    test('Reject promise', done => {
        const start = Readable.from(['reject']);
        const goal = new Goal();
        goal.build(goalChildren);
        
        goal.callback((error, message) => {
            expect(error).toBe('reject');
            expect(message).toBe(undefined);
            done();
        });
        
        start.pipe(goal);
    });

    test('Manage exceptions', done => {
        const start = Readable.from(['throw']);
        const goal = new Goal();
        goal.build(goalChildren);
        
        goal.callback((error, message) => {
            expect(error).toBe('A thrown Error');
            expect(message).toBe(undefined);
            done();
        });
        
        start.pipe(goal);
    });
})