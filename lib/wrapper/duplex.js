const {Duplex} = require('stream');
const {DataWrapper} = require('./data_wrapper.js');
const {Manager} = require('../manager/manager.js');

/**
 * Wrapper for _Duplex_ streams that knows what to do with {@link DataWrapper} messages.
 * @extends Duplex
 */
class DuplexWrapper extends Duplex {
    constructor(options) {
        options = {...options, readableObjectMode: true, writableObjectMode: true};
        super(options);
        this.options = options;
        this.__real__push = this.push;
        this.push = (payload) => {
            if(payload != null && this.goal) {
                payload = payload instanceof DataWrapper?payload.getChild(this):new DataWrapper(payload, this);
            }
            this.__real__push(payload);
        };
        if(this._write) {
            this.__real__write = this._write;
            this._write = (payload, encoding, cb) => {
                if(this.goal) {
                    payload = payload instanceof DataWrapper?payload.getChild(this):new DataWrapper(payload, this);
                }
                this.__real__write(payload, encoding, cb);
            };
        }
        if(this._writev) {
            this.__real__writev = this._writev;
            this._writev = (payloads, cb) => {
                if(this.goal) {
                    payloads = payloads.forEach(payload=>payload.chunk = payload.chunk instanceof DataWrapper?payload.chunk.getChild(this):new DataWrapper(payload.chunk, this));
                }
                this.__real__writev(payloads, cb);
            };
        }
        this.type = 'Duplex';
        if(this.options.name) Manager.set(this.type, this);
    }

}

module.exports.DuplexWrapper = DuplexWrapper;