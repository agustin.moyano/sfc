const {Transform} = require('stream');
const {DataWrapper} = require('./data_wrapper.js');
const {Manager} = require('../manager/manager.js');

/**
 * Wrapper for _Transform_ streams that knows what to do with {@link DataWrapper} messages.
 * @extends Transform
 */
class TransformWrapper extends Transform {
    constructor(options) {
        options = {...options, readableObjectMode: true, writableObjectMode: true};
        super(options);
        this.options = options;
        this.__real__push = this.push;
        this.push = (payload, encoding) => {
            // console.log(payload);
            if(payload != null && this.goal) {
                payload = payload instanceof DataWrapper?payload.getChild(this):new DataWrapper(payload, this);
            }
            this.__real__push(payload, encoding);
        };
        this.type = 'Transform';
        if(this.options.name) Manager.set(this.type, this);
    }

}

module.exports.TransformWrapper = TransformWrapper;