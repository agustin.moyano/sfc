const {Writable} = require('stream');
const {DataWrapper} = require('./data_wrapper.js');
const {Manager} = require('../manager/manager.js');

/**
 * Wrapper for _Writable_ streams that knows what to do with {@link DataWrapper} messages.
 * @extends Writable
 */
class WritableWrapper extends Writable {
    constructor(options) {
        options = {...options, objectMode: true};
        super(options);
        this.options = options;
        this.type = 'Writable';
        if(this.options.name) Manager.set(this.type, this);
    }

    write(payload, encoding, cb) {
        if(this.goal) {
            payload = payload instanceof DataWrapper?payload.getChild(this):new DataWrapper(payload, this);
        }
        super.write(payload, encoding, cb);
    }

    writev(payloads, cb) {
        if(this.goal) {
            payloads = payloads.forEach(payload=>payload.chunk = payload.chunk instanceof DataWrapper?payload.chunk.getChild(this):new DataWrapper(payload.chunk, this));
        }
        super.writev(payloads, cb);
    }

}

module.exports.WritableWrapper = WritableWrapper;