const {Readable} = require('stream');
const {DataWrapper} = require('./data_wrapper.js');
const {Manager} = require('../manager/manager.js');

/**
 * Wrapper for _Readable_ streams that knows what to do with {@link DataWrapper} messages.
 * @extends Readable
 */
class ReadableWrapper extends Readable {
    constructor(options) {
        options = {...options, objectMode: true};
        super(options);
        this.options = options;
        this.__real__push = this.push;
        this.push = (payload) => {
            if(payload != null && this.goal) {
                payload = payload instanceof DataWrapper?payload.getChild(this):new DataWrapper(payload, this);
            }
            this.__real__push(payload);
        };
        this.type = 'Readable';
        if(this.options.name) Manager.set(this.type, this);
    }

}

module.exports.ReadableWrapper = ReadableWrapper;