const uuid = require('uuid');
const isPlainObject = require('is-plain-object');
/**
 * Helper class that wrapps streaming payloads inside a [<code>Goal</code>](#Goal). It holds history of in which streams it was flown through.
 */
class DataWrapper {
    constructor(data, src) {
        this.setData(data);
        this.parent=[];
        this.id = uuid.v4();
        if(src.options.name && src.goal) {
            // src is a goal player
            this.map = {[src.type]: {[src.options.name]: this.id}};
        }
    }

    getChild(src) {
        let data = isPlainObject(this.data)?JSON.parse(JSON.stringify(this.data)):this.data;
        let n = new DataWrapper(data, src);
        n.parent.unshift(this);
        n.map = {...this.map, ...n.map};
        return n;
    }

    setParents(parents, data) {
        this.parent = parents;
        if(data) return this.setData(data);
        data = parents.map(parent => {
            this.map={...parent.map, ...this.map};
            return parent.data;
        });
        return this.setData(data);
    }

    setData(data) {
        this.data = data instanceof DataWrapper?data.data:data;
        return this;
    }
}

module.exports.DataWrapper = DataWrapper;