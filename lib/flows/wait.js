const {Readable} = require('stream');
const {Manager} = require('../manager/manager.js');
const {DataWrapper} = require('../wrapper/data_wrapper.js')

/**
 * Wait untill every source ended to emit end event.
 * 
 * @extends Readable
 */
class FlowWait extends Readable {

    /** 
     * Create a FlowWait stream
     * @param {object} options Global options.
     * @param {string} [options.name] Name for this stream.
    */
    constructor(options) {
        options = {...options, objectMode: true};
        super(options);
        this.options = options;
        this._sources = [];
        this._readableState.sync = false;
        this.type = 'FlowWait';
        if(this.options.name) Manager.set(this.type, this);

        this.on('pipe', (src)=>{
            this._sources.push(src);

            this.on('pause', ()=>{
                src.pause();
            });
            this.on('resume', ()=>{
                src.resume();
            });
        });
    }

    write(payload) {
        if(this.goal) {
            // Is goal player
            payload = new DataWrapper(null, this).setParents(payload);
        }
        if(!this.push(payload)) this.pause();
    }

    end(src) {
        if(this._sources.find(src => !src._readableState.ended)) return;
        this.push(null);
    }

    _read() {
        this.resume();
    }

}

module.exports.FlowWait = FlowWait;