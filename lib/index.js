const {FlowAll} = require('./flows/all.js');
const {FlowOne} = require('./flows/one.js');
const {FlowEach} = require('./flows/each.js');
const {FlowJoin} = require('./flows/join.js');
const {FlowHold} = require('./flows/hold.js');
const {FlowFirst} = require('./flows/first.js');
const {FlowWait} = require('./flows/wait.js');
const {Goal} = require('./goals/goals.js');
const {DataWrapper} = require('./wrapper/data_wrapper.js');
const {Manager} = require('./manager/manager.js');
const {Rule} = require('./rules/rules.js');
// const {RuleGroup} = require('./rules/group.js');
const {DuplexWrapper} = require('./wrapper/duplex.js');
const {ReadableWrapper} = require('./wrapper/readable.js');
const {TransformWrapper} = require('./wrapper/transform.js');
const {WritableWrapper} = require('./wrapper/writable.js');

/**
 * Condition function for flowing to a stream
 * @callback Condition
 * @param {(DataWrapper|any)} payload chunk that's flown through the stream
 * @returns {boolean} if true, condition is accepted and data is flown to stream
 */

/**
 * Payload processing method
 * @callback Process
 * @param {(DataWrapper|any)} payload chunk that's going to be processed somehow
 */

 /**
 * Thenable function
 * @callback Then
 * @param {(DataWrapper|any)} payload chunk that's going to be processed somehow
 * @returns {Promise}
 */

 /**
 * Thenable function
 * @callback Catch
 * @param {(DataWrapper|any)} error error message
 * @returns {Promise}
 */

 /**
 * Returns the identity of a message. It could be an internal id, or an id of a wrapped message.
 * @callback Identify
 * @param {(DataWrapper|any)} payload chunk that's flown through the stream
 * @returns {any} identity of the message
 */

 /**
  * Node style callback
  * @callback NodeCallback
  * @param {any} error
  * @param {any} data 
  */

  /**
   * Process rules. You may emit events to send data to streams.
   * @callback RuleProcess
   * @param (DataWrapper|any)} payload chunk that's flown through the stream
   * @returns {boolean|any|undefined}  
   */

module.exports.FlowAll = FlowAll;
module.exports.FlowOne = FlowOne;
module.exports.FlowEach = FlowEach;
module.exports.FlowHold = FlowHold;
module.exports.FlowJoin = FlowJoin;
module.exports.FlowFirst = FlowFirst;
module.exports.FlowWait = FlowWait;
module.exports.Goal = Goal;
module.exports.DataWrapper = DataWrapper;
module.exports.sfc = Manager;
module.exports.Rule = Rule;
// module.exports.RuleGroup = RuleGroup;
module.exports.Duplex = DuplexWrapper;
module.exports.Readable = ReadableWrapper;
module.exports.Transform = TransformWrapper;
module.exports.Writable = WritableWrapper;
